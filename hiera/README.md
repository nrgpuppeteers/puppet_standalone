# hiera directory

This directory contains YAML file(s) with parameters for puppet to look up prior to running any manifests.  It 
is referenced ../hiera.yaml as hiera's datadir.  Files in this directory would need to conform to the hiearchy
specified in hiera.yaml, e.g. common.yaml, etc.

Copy the demonstration file common.yaml.example to common.yaml for hiera to see it, and modify as needed.

Parameter values stored in these YAML files would be either be retrieved by puppet via automatic parameter
lookup, or explicitly with usage like hiera('generic::credentials::postgres_password').

This directory is by intention not under revision control, to avoid secrets stored in YAML files from being
accidentally checked into a public repo.

# puppet_standalone

## Overview

This repo contains puppet manifests and related files for stand-alone deployment of NRG tools.

At present, these manifests are written for Puppet v4.1.0.

## Installing Puppet-agent 
https://puppet.com/docs/puppet/4.10/install_linux.html

## Configuration

### r10k
Besides puppet v4, you'll need the r10k tool to retrieve dependent modules for the manifest(s) you want to 
run.  A Puppetfile is included in this repo, and you can create your own as needed.

```
sudo yum -y install ruby curl git openssl
sudo gem install r10k
r10k puppetfile install -v --puppetfile=./Puppetfile --moduledir=./modules
```
#### Note
The current version of ruby for puppet v.4 is v.2.1. The command above may try to install the latest version of r10k which leads the error below:
```
# gem install r10k
ERROR:  Error installing r10k:
	cri requires Ruby version ~> 2.3.
```

In that case, run the follwoing command for installing the correct verison of r10k:
```
/opt/puppetlabs/puppet/bin/gem install r10k --version=2.1
```

### hiera
This repo also has a minimal hiera.yaml config file, along with a demostration YAML file 
hiera/common.yaml.example.  Copy that file to hiera/common.yaml for hiera to see it.  You can add parameter 
values to hiera/common.yaml as needed, and/or modify hiera.yaml to expand the hierarchy.

Presently, the hiera directory contents are excluded from revision control, to help avoid accidentally 
checking secrets into the repo.  Better secrets management in hiera TBD.

Note hiera isn't required for running manifests in stand-alone mode.  Omit the "hiera_config" command line 
option if you don't need it.

## Run Puppet on Your Manifest
Once dependent modules have been downloaded and hiera parameter values filled in where needed, you can run 
puppet on your stand-alone manifest.

Example:
```
sudo puppet apply --hiera_config=./hiera.yaml --modulepath=./modules manifests/deploy_xnat17.pp
```

